# [We Have Moved To GitHub](https://github.com/JamesCJ60/Universal-x86-Tuning-Utility)

## AMD APU Tuning Utility

If you come across any issues or errors with AATU please open an issue or ping `@JamesCJ#2021` on [discord](https://discord.gg/M3hVqnT4pQ).

Thanks to https://github.com/FlyGoat/RyzenAdj and his author, ryzenadj.exe is included (windows only for now). 

The future roadmap of AMD APU Tuning Utility can be found here: https://amdaputuningutility.com/news/jul-2021/aaturoadmap.html

If you would like to support the development of AMD APU Tuning Utility, you can do so here: https://www.patreon.com/aatusoftware

As seen in: https://www.youtube.com/watch?v=9GfP8aaQPAg & https://www.youtube.com/watch?v=MdfrY8FO7o0

- [What is AATU?](#what-is-aatu)
- [Benefits](#benefits-of-using-aatu)
- [Installation](#installation)
- [Disclaimers & Cautions](#disclaimers-cautions)
- [Pre-requisite](#pre-requisite)

## What is AATU?
- It's a new Ryzen Mobile APU tuning utility created by one of the developers of [Ryzen Controller](https://gitlab.com/ryzen-controller-team/ryzen-controller).
- It's a little lightweight Ryzen Master for laptops that allows you to control the power limits of your APU.
- Works best on 2xxx, 3xxx, 4xxx and 5xxx series Ryzen Mobile.

## Benefits of using AATU
Benefits of using AMD APU Tuning Utility/Ryzen Controller on your laptop:
- You can gain anywhere from as little as 5% to as much as 35%+ extra performance for absolutely free without having to buy a new laptop, prolonging the life of your existing Zen-based mobile APU.
![alt text](https://cdn.discordapp.com/attachments/772164404598276135/870293764688715776/Screenshot_2021-07-29_at_14.png)
*stock power limits and results will vary depending on the laptop make, model and specifications. This data is here to prove that you can gain extra performance when allowing more power draw.

## Installation
- Go to [release page](https://gitlab.com/JamesCJ/amd-apu-tuning-utility/-/releases)
- Download the Windows Download file
- Read the [Installation and Important info.txt] file before use.. (do not need to do with V2 and up)
- Install the bundled SqlLocalDB.msi installer included in the AATU download if you haven't done so already (also not needed with V2 and up)
- Have fun!!!

## Disclaimers & Cautions

- If you intend to use AMD APU Tuning Utility in a video/text post online (e.g. YouTube, Reddit) please credit the AMD APU Tuning Utility team by linking to the AMD APU Tuning Utility GitLab release page! We ask this so that viewers/readers can download the software from a trusted source and so the developers get the proper recognition for their work.
- AMD APU Tuning Utility Team is not liable for any damages that my occur from using AMD APU Tuning Utility or RyzenADJ, Please use at your own risk!
- "AMD", "APU", "Ryzen", and "AMD Ryzen" are trademarked by and belong to Advanced Micro Devices, Inc. "ROG", and "Armoury Crate" are trademarked by and belong to AsusTek Computer, Inc. AMD APU Tuning Utility Team makes no claims to these assets and uses them for informational purposes only.

## Pre-requisite (Only for V1)
- LocalDB 2017 or up for the preset system which can be found [here](https://go.microsoft.com/fwlink/?LinkID=866658) (This is included in the AATU download as SqlLocalDB.msi, also not needed with V2 and up)
